import argparse
import json
import random
import sys

import tensorflow as tf
from tensorflow import keras
import numpy as np
from data_prep import jigs, replace_mapping
from generators import KerasTuneGenerator
import os
import pickle

# True if the model predicts window size characters, False if it only predicts one
PREDICT_SEQUENCE = False
NUM_FEATURES = 10
USE_LSTM = True


def read_chars(filename):
    with tf.gfile.GFile(filename) as f:
        return list(f.read())


def create_model(vocab_size, input_length, dropout=True):
    if USE_LSTM:
        model = keras.Sequential([
            keras.layers.Embedding(vocab_size, NUM_FEATURES, input_length=input_length, batch_input_shape=(1, input_length)),
            keras.layers.LSTM(30, return_sequences=True, stateful=True),
            keras.layers.LSTM(30, return_sequences=True, stateful=True),
            keras.layers.Dropout(0.1),
            keras.layers.Flatten(),
            keras.layers.Dense(vocab_size),
            keras.layers.Activation('softmax'),
            keras.layers.Dropout(0.1),
        ])
        lr=0.0001
    else:
        model = keras.Sequential([
            keras.layers.Embedding(vocab_size, NUM_FEATURES, input_length=input_length, batch_input_shape=(1, input_length)),
            keras.layers.Dense(55),
            keras.layers.Dense(55),
            keras.layers.Dropout(0.1),
            keras.layers.Flatten(),
            keras.layers.Dense(vocab_size),
            keras.layers.Activation('softmax'),
            keras.layers.Dropout(0.1),
        ])
        lr=0.00001
    model.compile(
        loss='categorical_crossentropy',
        optimizer=keras.optimizers.RMSprop(lr=lr),
        metrics=['categorical_accuracy']
    )

    return model


def test_model(model, vocab_size, test_data, decode_dict, window_size):
    # print(model.layers[1].get_weights())
    # print(model.layers[2].get_weights())

    # Set this to true for the model to be evaluated on an existing tune rather
    # than using it's own output as input
    seed_whole_tune = False

    num_predict = 250
    dummy_iters = random.randrange(0, 20)
    example_training_generator_k = KerasTuneGenerator(test_data, window_size, 1, vocab_size)
    example_training_generator = example_training_generator_k.generate(return_sequence=PREDICT_SEQUENCE)

    print("Testing...", dummy_iters)
    for i in range(dummy_iters):
        next(example_training_generator)
        while not example_training_generator_k.tune_ended:
            d = next(example_training_generator)
    pred_print_out = "Predicted chars: \n"
    data_pairs = next(example_training_generator)
    data = data_pairs[0]
    print("Seed: '{}'".format("".join([str(decode_dict[d]) for d in data[0]])))
    for i in range(num_predict):
        if seed_whole_tune:
            prediction = model.predict(next(example_training_generator)[0])
        else:
            prediction = model.predict(data)
        if PREDICT_SEQUENCE:
            predict_word = np.argmax(prediction[:, window_size-1, :])
        else:
            predict_word = np.argmax(prediction)
        pred_print_out += str(decode_dict[predict_word])
        data = np.array([np.concatenate((data[0][1:], [predict_word]))])
    print(pred_print_out)


def one_hot_to_num(arr):
    for i in range(len(arr)):
        if arr[i] == 1:
            return i


class ResetStateAfterTune(keras.callbacks.Callback):
    def __init__(self, gen):
        super().__init__()
        self.gen = gen

    def on_batch_end(self, batch, logs={}):
        if self.gen.tune_ended:
            self.model.reset_states()


def count_samples(iter):
    samples = 0
    for _ in iter:
        samples += 1
    return samples


def train_model(manager, model, train, valid, vocab_size, window_size, epochs, progress_name):
    batch_size = 1

    train_gen = KerasTuneGenerator(train, window_size, batch_size, vocab_size)
    valid_gen = KerasTuneGenerator(valid, window_size, batch_size, vocab_size)

    print("Counting samples... ", end="")
    sys.stdout.flush()
    samples_train = count_samples(train_gen.generate())
    print("Training:", samples_train, end='')
    sys.stdout.flush()
    samples_valid = count_samples(valid_gen.generate())
    print(" Validation:", samples_valid)

    reset_state_after_tune = ResetStateAfterTune(train_gen)
    model_history = {
        "loss": [],
        "categorical_accuracy": [],
        "val_loss": [],
        "val_categorical_accuracy": []
    }
    # The best validation accuracy we've seen so far. Whenever we create a model
    # that does better than this, save that model to a file.
    best_val_acc = 0
    save_at = 1
    for i in range(epochs):
        epoch = i+1
        print("Epoch {}/{}".format(epoch, epochs))
        history = model.fit_generator(
            train_gen.generate(return_sequence=PREDICT_SEQUENCE),
            samples_train // batch_size,
            epochs=1,
            validation_data=valid_gen.generate(return_sequence=PREDICT_SEQUENCE),
            validation_steps=samples_valid // batch_size,
            shuffle=False,
            callbacks=[reset_state_after_tune] # Only required for LSTM
        )
        val_acc = history.history['val_categorical_accuracy'][0]
        for key in model_history.keys():
            model_history[key].append(float(history.history[key][0]))
        model.reset_states()
        if progress_name is not None:
            with open(os.path.join("training", f"{progress_name}-history.json"), 'w') as f:
                json.dump(model_history, f)
            if val_acc > best_val_acc or epoch == save_at:
                manager.save_model(f"{progress_name}-{epoch}")
                if val_acc > best_val_acc:
                    best_val_acc = val_acc
                if epoch == save_at:
                    save_at *= 2
    return model_history


class MLManager(object):
    def __init__(self, dataset):
        (
            self.train_gen,
            self.valid_gen,
            self.test_gen,
            self.decode_dict
        ) = jigs.load_data("data", dataset, verbose=0)
        self.vocab_size = len(self.decode_dict)
        self.window_size = 10
        self.model = None
        self.decode_dict[len(self.decode_dict) - 1] = '-'
        self.history = None

    def set_window_size(self, steps):
        self.window_size = steps
        return self

    def create_model(self):
        self.model = create_model(self.vocab_size, self.window_size)

    def load_weights(self, checkpoint):
        self.model.load_weights(checkpoint)
        return self

    def test_model(self):
        assert self.model is not None
        test_model(
            self.model,
            self.vocab_size,
            self.test_gen,
            self.decode_dict,
            self.window_size
        )

    def train_model(self, epochs=10, progress_name=None):
        assert self.model is not None
        self.history = train_model(
            self,
            self.model,
            self.train_gen,
            self.valid_gen,
            self.vocab_size,
            self.window_size,
            epochs,
            progress_name
        )

    def save_model_png(self, name):
        keras.utils.plot_model(self.model, to_file=f'training/{name}.png')

    def save_model(self, name):
        model_data = {
            "weights": [l.get_weights() for l in self.model.layers
                        if not isinstance(l, keras.layers.Dropout)],
            "mapping": self.decode_dict,
            "history": self.history,
            "config": self.model.get_config()
        }
        with open(os.path.join("training", f"{name}.pkl"), 'wb') as f:
            pickle.dump(model_data, f)

    def load_model(self, name):
        with open(os.path.join("training", f"{name}.pkl"), 'rb') as f:
            model_data = pickle.load(f)
        # if "config" in model_data:
        #     self.model = keras.Sequential.from_config(model_data['config'])
        for i, layer in enumerate(filter(lambda l: not isinstance(l, keras.layers.Dropout), self.model.layers)):
            layer.set_weights(model_data['weights'][i])
        self.decode_dict = model_data['mapping']
        self.history = model_data['history']

    def load_model_legacy(self, name):
        nzfile = np.load(os.path.join("training", "{}.npz".format(name)))
        for i, layer in enumerate(self.model.layers):
            if isinstance(layer, keras.layers.Dropout):
                continue
            layer.set_weights(nzfile["arr_{}".format(i)])
        with open(os.path.join("training", "{}-mapping.json".format(name)), 'r') as f:
            self.decode_dict = {int(k): v for (k, v) in json.load(f).items()}


def print_help():
    print(f"{__file__} dataset [test]")


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('dataset')
    parser.add_argument('-m', '--model', help='The name of the model to load/save')
    parser.add_argument('-e', '--epochs', help='The number of epochs to train for', type=int)
    parser.add_argument('-p', '--progress', help='Save model progress during training',
                        action='store_const', const=True, default=False)
    parser.add_argument('mode', choices=['train', 'test'])

    args = parser.parse_args()

    dataset = args.dataset
    model_name = args.model or dataset
    epochs = args.epochs or 5
    save_progress = args.progress

    manager = MLManager(dataset) \
        .set_window_size(8)

    manager.create_model()

    if args.mode == "test":
        manager.load_model(model_name)
        manager.model.summary()

        manager.test_model()
    else:
        manager.model.summary()
        progress_name = None
        if save_progress:
            progress_name = model_name
        try:
            manager.save_model_png(model_name)
        except ImportError:
            print("WARNING: Graphviz not installed, skipping image generation...")
        manager.train_model(epochs=epochs, progress_name=progress_name)
        manager.save_model(model_name)
        manager.test_model()


if __name__ == "__main__":
    main()
