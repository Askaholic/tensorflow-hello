import argparse
from matplotlib import pyplot
from file_system import load_model, training_dir
import json


def plot(model_name, history):
    pyplot.figure(1)
    pyplot.plot(history['categorical_accuracy'])
    pyplot.plot(history['val_categorical_accuracy'])
    pyplot.title(f'{model_name} accuraccy')
    pyplot.ylabel('Accuracy')
    pyplot.xlabel('Epochs')
    pyplot.legend(['Train', 'Test'], loc='lower right')

    pyplot.figure(2)
    pyplot.plot(history['loss'])
    pyplot.plot(history['val_loss'])
    pyplot.title(f'{model_name} loss')
    pyplot.ylabel('Loss')
    pyplot.xlabel('Epochs')
    pyplot.legend(['Train', 'Test'], loc='upper right')
    pyplot.show()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('model', help="The name of the model")
    parser.add_argument('-p', '--partial', help='Load history from the json file. Useful if the training run did not complete.',
                        action='store_const', const=True, default=False)

    args = parser.parse_args()
    model = args.model
    load_partial = args.partial

    try:
        if load_partial:
            with open(training_dir(f'{model}-history.json')) as f:
                model_data = {
                    "history": json.load(f)
                }
        else:
            model_data = load_model(model)
        plot(model, model_data['history'])
    except FileNotFoundError:
        print("ERROR: That model doesn't exist!")
    except Exception:
        print("ERROR: That model does not have a readable history!")
