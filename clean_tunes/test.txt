"tune","setting","name","type","meter","mode","abc","date","username"
"15458","28943","6/8 De Rapídes","jig","6/8","Dmajor","| d2c B2A | F2A DEF | G2G GFG | BAG ABA |
| d2c B2A | F2A DEF | G2G ABc| dAF D3 :|
| A,3 D3 | FEF FEF | GFG GFG | BAG ABA |
| A,3 D3 | FEF FEF | GFG GFG | BGE D3 :|","2016-07-03 19:56:05","Jesse"
"3897","31133","A Visit To Ireland","jig","6/8","Dmajor","|:ABA AFD|FAA ABc|dcB AGF|GEE E3|
ABA AFD|FAA ABc|dcd EFG|FDD D3:|
|:dcd ede|fed cBA|dcB AGF|GEE E3|
dcd ede|fed cBA|dcd EFG|FDD D3:|","2017-10-08 19:43:41","fluther"
"4742","4742","Across The Bridge To Connaught","jig","6/8","Dmajor","|:fdd edB|Add edB|Add dcd|efg bag|
fdd edB|Add edB|Agf edc|d3 d2e:|
|:fed fed|cag a2g|fed fed|cde A2e|
fed fed|cag a2g|fed edc|d3 d2e:|","2005-07-17 03:01:31","gian marco"
"11086","11086","Aggie's Cauldron","jig","6/8","Dmajor","|: E2 B BAB| BdB BAF| E2 B BAB| BdB A2 B|
E2 B BAB| BdB BAF| BdB BAF| FED E2 B :|
e2 e efe| efe dBA| e2 e efe| dBA B2 A|
e2 e efe| efe dBA| dcB BAF| FED E3 :|","2011-02-22 20:48:08","Kavik"
"12035","12035","Alex Doig's","jig","6/8","Dmajor","f2e dcB | A2d F2A | G2B EFG | Ace gfe |
f2e dcB | ABd F2A | G2e cBc | d3 d3 :|
|: e2c A2A | f2d A2A | ~g3 ~f3 | efc ABc |
e2c A2A | f2d A2f | gfe ABc | dAF D3 :|","2012-06-30 09:10:23","Dr. Tøm"
"15957","30032","Alex's Haircut","jig","6/8","Dmajor","Add dce|dff egg|faa g2 e|fed ecA|
GBB BdB|Add dce|dff faa|gfg e2 f|
Add dce|dff egg|faa g2 e|fed ecA|
GBB BdB|Add dce|dff age|edc d2 B||
||:AcA dAe|AdA eAf|AgA fAe|AdA edc|
AcA dAe|AdA eAf|Agg Aff|edc d2 B:||
","2017-03-12 18:21:39","iris eve"
"8783","8783","Alicia Kate Parker's","jig","6/8","Dmajor","|: dAf e3 | dBA BAF | AFB Adf | efd e2 f |
dAF efd | dBA BAF | EFA Bdf | dBA B3 :|
|: b3 efb | afe dBA | FAA cef | e3 dea |
b3 efa | fed AFA | dAf edB | AFA B3 :|","2008-08-04 08:45:26","quiet desperation"
"15866","29841","Almost Blue","jig","6/8","Dmajor","=F^FA ^ABd|=f^fa fed|fed ^ABd|G6|=c^ce A2A|
=c^ce A2A|f2a ^f=fe|d2- d- d2 A|=F^FA ^ABd|
=f^fa fed|fed ^ABd|G6|^EFA =cBA|Ace gfe|B=cB AB^c| d6:|
a^ga =c'2a|a^ga =f2 e|d2 d =f2 ^f|g6|e^de a3|
c=c^c c=c^c|Ace gfe|^f6|a^ga =c'2a|a^ga =f2 e|
d2 d =f2 ^f|g6|=c^ce ^efa|=c^ce ^efa|B=cB AB^c| d6 :|","2017-02-14 02:07:46","David Meredith"
"13805","24769","An Páistín Fionn","jig","6/8","Dmajor","|:A2d dcd|fed efe|ded dAB|=c2B AFG|
A2d dcd|fed efe|dcA GEA|D3 D3||
|:FGA dAA|BGG AFD|FGA cde|d3 d2D|
FGA cAG|def gfe|dcA GEA|D3 D3||","2014-08-24 05:50:14","JACKB"
"10598","10598","An Tunnag","jig","6/8","Dmajor","A,2 D DEF|E2 D DEF| E2 D DEF| E2 D EFF|
A,2 D DEF|E2 D DEF| E2 D FED|B,2 A, B,DD:|
A,2 A F2E|FDD D2E|FDD D2E|F2E FAA|
A,2A F2E|FDD D2E|F2E FED|B,2A, B,DD:|
Acc cef|e2c cef|e2c cef|e2c eff|
Acc cef|e2c cef|e2c fec|B2A Bcc:|
A2a f2 e|fea f2e|fea f2e|fea fea|
A2a f2e|fea f2e|fea fec|B2A Bcc:|","2010-07-16 14:40:53","bosca-ceol"
"3937","3937","Angler's","jig","6/8","Dmajor","|:dAF DFA|dAF DFA|GFG EFD|CDE A,FA|dAF DFA|dAF DFA|GFG A,EC|DEC D3:|
|:AFD AFD|BGD BGD|AFD AFD|GFG ECA,|AFD AFD|BGD BGD|AFD ECA,|DECD3||","2004-11-27 13:36:12","harpalaska"
"3937","16817","Angler's","jig","6/8","Dmajor","|:dAF DFA|dAF DFA|GFG EFD|CDE A,FA|dAF DFA|dAF DFA|GFG A,EC|DEC D3:|
|:AFD AFD|BGD BGD|AFD AFD|GFG ECA,|AFD AFD|BGD BGD|AFD ECA,|DECD3||","2006-03-25 22:16:34","harpalaska"
"4899","17327","Angry Peeler, The","jig","6/8","Dmajor","f2e d2A|FAA BAF|ABd edB|efe ede|
fge d2A|FAA BAF|ABd edB|ded d2e:|
|:fef a2f|abf afe|d2f edB|efe ede|
fef a2f|abf afe|d2f edB|ded d2e:|","2007-09-19 10:06:25","Dr. Dow"
"3274","3274","Aoife's","jig","6/8","Dmajor","FAd GBd|FGA AGE|FAd GBd|AFD EEE|
FAd G2d|FGA AGE|FAd fef|gec d2z|
FAd GBd|FAd EAc|FAd GBd|AFD EEE|
FAd G2d|FGA AGE|FAd fef|gec d2f|
afd bgd|afd AGE|DFA GBd|AFD EEE|
afd b2g|afd AGE|DFA GBd|AGE D2z|
afd bgd|afd AGE|FGA GAB|AFD EEE|
FAd G2d|FGA AGE|FAd fef|gec d2z|","2004-07-12 19:37:20","Peter Piper"
"14503","26715","As Fadas De Estraño Nome","jig","6/8","Dmajor","DFA d2 d| BdA BAF | DEF ABA | E2A FED | 
DFA d2d | BdA BAF | DEF ABA | EAF D3 :|
dAd dAd|F2E FED |Add BAB | F2A B3 |
Add Bdd | Add FEF | DDDA2B | FFFE2F | D3 :|
FGF Fd=c | BAG AB=c | FGF Fd=c | BAGA3 | 
FGA FDD | GEE FDD|=cBA AGF | E2FD3 :|","2015-06-24 12:35:50","Cyril Johnson"
"14503","26720","As Fadas De Estraño Nome","jig","6/8","Dmajor","|:DFA d2 d| BdA BAF | DEF ABA | E2A FED |
DFA d2d | BdA BAF | DEF ABA | EAF D3 :||
|:dAd dAd|F2E FED |Add BAB | F2A B3 |
Add Bdd | Add FEF | D3 A2B | F3 D3 :||
|:FGF Fd=c | BAG AB=c | FGF Fd=c | BAGA3 |
FGA FDD | GEE FDD|=cBA AGF | E2FD3 :||","2015-06-26 01:31:12","JACKB"
"11274","11274","At It Again","jig","6/8","Dmajor","A,|D3 FEF|DFA BAF|A2d BAF|A3-A zd-|
dBA FBA|FED B,2A,|D2F EDB,|D3-D2z|
d2d BAF|A2B AFD|d2d BAF|A3-A de|
fed BdB|AFD B,2A,|D2F EDB,|D3-D2|]","2011-06-02 18:46:04","Johnered66"
"11154","11154","Aughnahoo","jig","6/8","Dmajor","|: ABA F2D|ABA FAD| GFG EG=C|GFG AFG|
ABA F2D|ABA FAd| efd edB|AFD D3 :|
a3 afd|efg agf|gee bee|gfe efg|
aba afd|ede g=cc|dBA BAF|AFD D3
a3 afd|efg agf|gee bee|gfe efg|
aba fgf|efe fga| f2d edB|AFD D3




","2011-04-05 06:36:39","DerryMusicMan"
"2607","27209","Australian Waters","jig","6/8","Dmajor","d2D FED | =c3 ed^c | dAF GFE | FED fge |
d2D FED | =c3 ed^c | dAF GFE | FDC D3 :|
|: dcd efg | fed cBA | dcd efg | fdc def |
gbg faf | ecA ABc | dAF GFE | FDC DFA :|","2015-09-20 21:40:04","Ian Varley"
"2118","2118","Banks Of Allan, The","jig","6/8","Dmajor","|:~F3 FED|~F3 AFA|Bed Aed|Bde AFD|
~F3 FED|~F3 AFA|Bed AFD|FEE EFG:|
|:eff fed|efa afd|eff fed|g2a bag|
fed ~e3|d2e fed|Bed AFD|FEE EFG:|","2003-11-03 23:52:46","gian marco"
"2118","15502","Banks Of Allan, The","jig","6/8","Dmajor","|:cdc cBA|cee e2a|faa eaa|faa eaa|
cdc cBA|cee e2a|faf ecA|cBB B3:|
|:cdc cBA|cee e3|cdc cBA|dff f2e|
cde Bcd|Acc cBA|faf ecA|cBB B3:|","2003-02-28 19:01:35","dafydd"
"8639","19584","Banks Of The Allan, The","jig","6/8","Dmajor","FGF FED | FAA A2d | Bdd Add | Bdd Add | FGF FED | FAA A2d | BdB AFD | EDD D3 :||
fdf fed | faa a2g | fgf fed | gbb b2a | fga efg | def fed | BdB AFD | EDD D3 :||","2008-06-21 11:13:48","hetty"
"1804","1804","Banks Of The Tar, The","jig","6/8","Dmajor","|:dcd AFD|DFA dcd|~=c3 AGE|EDE GA=c|
dcd AFD|DFA dcd|f2a gec|edc d2A:|
|:f2a gec|ABA GED|f2a gec|ea^g a2e|
f2a gec|ABA GED|=CEG ~=c3|AGE D2d:|","2003-07-06 20:30:58","Mikea"
"12646","21281","Bar Behind The Maid, The","jig","6/8","Dmajor","|:FAF AFD | FAF Ade | fBA Bcd | faf edB |
AFB ADE | AFB Ade | fBA Bcd | AFE D3:|
|:fag fde | fda fda | efg bef | geb geb |
faf bfa | def ede | eBA Bcd | AFE D3:|

","2013-03-30 10:34:40","fluther"
"12658","21332","Bashful Maid, The","jig","6/8","Dmajor","|: F2A AFA | AFA d2A | GBB FAA | ~E3 EFG |
FAA AFA | AFA d2B | ABc dcB | AFD D3 :|
|: ecA Acd | ecA d2A | GBB FAA | ~E3 Ecd |
ecA Acd | ecA d2B | ABc dcB | AFD D3 :|","2013-04-07 12:28:18","stanton135"
"382","382","Battering Ram, The","jig","6/8","Dmajor","|: dBG BAG | dBG G2B | dBG AGE | GFD D2B |
dBG BAG | BdB BAG | AGA BAB | GED D2B :|
|: deg aga | bge edB | deg aga | bge ega |
bag age | ged ege | dAG AGE | GED D2B :|
|: B2G A2G | BGE DdC | BAG AGE | GED DdC |
B2G A2G | BdB BAG | AGA BAB | GED D3 :|","2001-11-17 04:01:52","Josh Kane"
"382","13209","Battering Ram, The","jig","6/8","Dmajor","B2G AGA | Bdd BAG | AGA BAB | GED GAB.","2011-05-11 10:52:52","DerryMusicMan"
"1095","1095","Beef To The Heel","jig","6/8","Dmajor","|:Adf fdf|Ace ece|Bcd efe|dcB A3|
Adf fdf|Ace ece|Bcd efe|d2c d3:||
|:fed fed|ecA ecA|Bcd efe|dcB A3|
fed fed|ecA ecA|Bcd efe|d2c d3:||
|:Adf a^ga|gec gec|Adf a^gg|gec Ace|
Adf a^ga|gec gec|Bcd efe|d2c d3:||","2002-10-29 17:48:22","Finito"
"1152","1152","Bell's Favourite","jig","6/8","Dmajor","|:FEF GFG|BAF A2d|cde ABc|dcB A2G|
FEF GFG|BAF A2d|cde ABc|d3 d3:|
|:a2a fdf|gfg e2d|cde ABc|dcB Afg|
aba fdf|gfg e2d|cde ABc|d3 d3:||","2002-11-23 15:09:04","fidicen"
"5001","5001","Benedict's Fancy","jig","6/8","Dmajor","GGG dBA|GAB AFD|GGG dBA|BAA A3|
GGG dBA|GAB AFD|GGG dBG|AGG G3:|
dgg fed|dgg f3|dgg dBA|BAA A3|
dgg fed|dgg e3|dgg dBG|AGG G3:|","2005-09-25 13:52:25","Innocent Bystander"
"15400","28793","Biddy McGhee","jig","6/8","Dmajor","| afd edB | dBA AFA | d2f ede | ~f3 efg |
afd edB | dBA AFA | d2f ede | gdc d3 :|:
faf ~g3 | faf fed | faf g2g | afd efd |
faf ~g3 | faf fed | f2f ede | fdc d3 :|


","2016-05-17 16:24:08","gian marco"
"3680","3680","Billy Borndrunk's","jig","6/8","Dmajor","|dcd ede|fef gfg|~a3 ged|cde ABc|
~d3 ede|~f3 gfg|afa gec|edc ~d3:|
efe ecA|ecA efg|aba gag|fed ~e3|
efe ecA|ecA efg|~a3 gec|edc ~d3:|","2003-08-28 21:31:44","dafydd"
"4102","16889","Billy Pigg's","jig","6/8","Dmajor","FDF AFD | ABc dAG | FDF AFD | EB,E GFE |
FDF ABc | dfe dcd | cBA GEC | EDD D2 :|
dcd AFD | dcd ecA | =cBc GEC | =cBc dBG |
dcd ABc | dfe dcd | cBA GEC | EDD D2 :|","2007-04-18 04:19:43","ceolachan"
"10966","10966","Black Dog","jig","6/8","Dmajor","F2 F A2 F | GFG E2 E| c2 c B2 A | ^GAB A2 A|
d2 f edc| B2 c d2 B| A2 F GFE | D3 A3 :|
|: d2 d edc | B2 A F2 A| G2 E c2 e| dcB A2 A|
d2 d edc | B2 A F2 A| G2 E c2 e| d3 d z2 :|","2011-01-09 12:33:54","Jim Quail"
"10966","20537","Black Dog","jig","6/8","Dmajor","F2 F A2 F | GFG E2 E| c2 c B2 A | ^GAB A2 A|
d2 f edc| B2 c d2 B| A2 F GFE | D3 A3 :|
|: d2 d edc | B2 A F2 A| G2 E c2 e| dcB A2 A|
d2 d edc | B2 A F2 A| G2 E c2 e| d3 d z2 :|","2011-01-09 12:36:26","Jim Quail"
"5","30071","Blarney Pilgrim, The","jig","6/8","Dmajor","ABd ABd|e2e efg| fed edB|ded dBA|
ABd ABd|e2e efg|fed edB|ded e3:|
aaa afd| edd fed|aaa afd| ede fab|
d'd'd' bad'|a2a fde|fff fed|edB d3:|","2017-03-19 00:57:54","RiteRight"
"7863","7863","Bobby Gardiner's","jig","6/8","Dmajor","FAA AFD|FAA AFD|GBB BAB|BAB Bcd|
FAA AFD|FAB AFD|FAc dBA|AFD EDE:|
FED AFD|BdB AFD|FED AFD|BAG FGE|
FED AFD|BdB AFD|FAc dBA|AFD EDE:|","2007-10-14 14:47:15","Will Harmon"
"1958","1958","Bobby Gardner's","jig","6/8","Dmajor","DFA A2D|FA=c B2A|GBB B2A|GAB dAG|
FA^G A2D|FAA ABc|d2B AFA|dAF E2D:|
FDD ADD|B3 ABd|FDD ADD|FED FED|
FDD ADD|B3 ABc|d2B AFA|dAF E2D:|","2003-09-08 14:31:16","whistlemanhimself"
