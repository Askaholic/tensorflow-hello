import csv
from data_prep.jigs import is_acceptable


def main():
    file = "/home/dope/Documents/TheSession-data/csv/tunes.csv"

    validation_data_percent = .10

    with open(file) as f:
        with open(os.path.join("data", "clean_tunes_train.csv"), "w") as f_train:
            with open(os.path.join("data", "clean_tunes_valid.csv"), "w") as f_valid:
                (reader, total) = _get_reader_and_count(f)
                writer_train = _get_writer(f_train)
                writer_valid = _get_writer(f_valid)
                header = next(reader)

                print("Header", header)
                writer_train.writerow(header)
                writer_valid.writerow(header)
                for i, row in enumerate(reader):
                    if not row or not is_acceptable(row[3:7]):
                        continue
                    if i < total * validation_data_percent:
                        writer_valid.writerow(row)
                    else:
                        writer_train.writerow(row)


def _get_reader(f):
    return csv.reader(f, delimiter=",", skipinitialspace=True, escapechar="\\")


def _get_writer(f):
    return csv.writer(f, delimiter=",", skipinitialspace=True, escapechar="\\",
                      quotechar='"', quoting=csv.QUOTE_NONNUMERIC)


def _get_reader_and_count(f):
    reader = _get_reader(f)
    header = next(reader)
    total = sum(1 for row in reader)
    f.seek(0)
    reader = _get_reader(f)

    return (reader, total)


if __name__ == '__main__':
    main()
