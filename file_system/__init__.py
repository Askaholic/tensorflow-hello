import os
import pickle


def load_model(name):
    with open(training_dir(f"{name}.pkl"), 'rb') as f:
        model_data = pickle.load(f)
    return model_data


def training_dir(file_name):
    return os.path.join("training", file_name)
