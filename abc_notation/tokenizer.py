class Note(object):
    def __init__(self, pitch, accidental, duration=1):
        self.pitch = pitch
        self.accidental = accidental
        self.duration = duration

    def add_pitch_modifier(self, modifier):
        last_mod = self.pitch[-1]
        if last_mod in ",'" and last_mod != modifier:
            self.pitch = self.pitch[:-1]
        else:
            self.pitch += modifier

    def __eq__(self, other):
        if self.__class__ != other.__class__:
            return False
        if self.accidental != other.accidental:
            return False
        if self.pitch != other.pitch:
            return False
        if self.duration != other.duration:
            return False

        return True

    def __repr__(self):
        return "Note('{}', '{}', {})".format(self.pitch, self.accidental, self.duration)

    def __str__(self):
        return "{}{}{}".format(self.accidental, self.pitch, self.duration if self.duration != 1 else '')

    def __hash__(self):
        return hash(str(self))


class Divider(object):
    def __eq__(self, other):
        return self.__class__ == other.__class__

    def __repr__(self):
        return "Divider()"

    def __str__(self):
        return "|"

    def __hash__(self):
        return hash(str(self))


def tokenize_notes(string):
    current_token = None
    accidental = ''
    tokens = []
    for c in string:
        if c in '|[]:':
            if isinstance(current_token, Note):
                tokens.append(current_token)
            current_token = Divider()
            accidental = ''
            continue
        if c in ' !\n~.\\-\t$':
            continue
        if c in 'abcdefgzABCDEFGZ':
            if current_token:
                tokens.append(current_token)
            current_token = Note(c, accidental)
            accidental = ''
            continue
        if c in '=_^':
            accidental = c
            continue
        if not current_token:
            continue
        if c in ",'":
            current_token.add_pitch_modifier(c)
        elif c in "23456789":
            if not isinstance(current_token, Note):
                raise Exception("Parse Error", c)
            for _ in range(int(c) - 1):
                tokens.append(current_token)
        else:
            raise Exception("Parse Error", c)
    return tokens


def parse_jig(token_iter, ignore_pickup=False):
    MEASURE_DURATION = 6
    measure = []
    tune = []
    first_measure = True
    for t in token_iter:
        if isinstance(t, Divider) and measure:
            measure_duration = get_measure_duration(measure)
            if measure_duration != MEASURE_DURATION:
                # Check if this is the pickup measure
                if not ignore_pickup and first_measure:
                    first_measure = False
                    measure = []
                    continue
                raise Exception("Measure too {}! {}".format(
                    "short" if measure_duration < MEASURE_DURATION else "long",
                    "".join([str(n) for n in measure]))
                )
            tune.append(measure)
            measure = []
        elif isinstance(t, Note):
            measure.append(t)
    return tune


def get_measure_duration(measure):
    i = 0
    for note in measure:
        i += note.duration
    return i
