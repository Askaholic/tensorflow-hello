import numpy as np
from tensorflow import keras
import csv
from data_prep import replace_mapping


class KerasTuneGenerator(object):
    def __init__(self, data_iterator, window_size, batch_size, vocab_size):
        # An iterator which yields a list of charachters on a tune by tune basis
        self.data = data_iterator
        self.window_size = window_size
        self.batch_size = batch_size
        self.vocab_size = vocab_size
        self.tune_ended = False

    def generate(self, *, return_sequence=True):
        """
        :param return_sequence: If True, yeilds an entire window as expected
            output. If False, only yields the next character.
        """
        x = np.zeros((self.batch_size, self.window_size))
        y = np.zeros((self.batch_size, self.window_size, self.vocab_size)) \
            if return_sequence else \
            np.zeros((self.batch_size, self.vocab_size))

        total_tunes = 0
        for tune in self.data.generate():
            total_tunes += 1
            self.tune_ended = False
            tune = iter(tune)
            # Something like: [None, 1, 2, 3, 4, ...] where then len is window_size + 1
            data = [None] + [next(tune) for d in range(self.window_size)]

            while True:
                try:
                    for i in range(self.batch_size):
                        data = data[1:] + [next(tune)]
                        x[i, :] = data[:-1]
                        if return_sequence:
                            temp_y = data[1:]
                            # convert all of temp_y into a one hot representation
                            y[i, :, :] = keras.utils.to_categorical(temp_y, num_classes=self.vocab_size)
                        else:
                            y[i] = keras.utils.to_categorical(data[-1], num_classes=self.vocab_size)
                except StopIteration:
                    self.tune_ended = True

                yield x, y
                if self.tune_ended:
                    break


class TuneGenerator(object):
    def __init__(self, filename):
        self.file = filename

    def generate(self):
        with open(self.file, encoding='utf-8') as f:
            reader = csv.reader(f, delimiter=",", skipinitialspace=True, escapechar="\\")
            next(reader)
            while True:
                try:
                    row = next(reader)
                    if not row:
                        continue
                    yield row[3:7]
                except UnicodeDecodeError:
                    continue
                except StopIteration:
                    break


class TuneFilter(object):
    def __init__(self, tune_generator, filter_fn):
        self.gen = tune_generator
        self.filter = filter_fn

    def generate(self):
        for tune in self.gen.generate():
            if not self.filter(tune):
                continue
            yield tune


class TransformGenerator(object):
    def __init__(self, tune_generator, transform_fn):
        self.gen = tune_generator
        self.transform = transform_fn

    def generate(self):
        for tune in self.gen.generate():
            yield self.transform(tune)


class TuneDataGenerator(object):
    def __init__(self, tune_generator, vocabulary):
        self.gen = tune_generator
        self.vocab = vocabulary

    def generate(self):
        for tune in self.gen.generate():
            yield replace_mapping(tune, self.vocab)
