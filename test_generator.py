from data_prep import jigs, replace_mapping
from abc_notation import parse_jig
from generators import KerasTuneGenerator, TransformGenerator
from data_prep import id_mapping_from_iterable


def main():
    gen = jigs.get_tune_generator("/home/dope/Documents/TheSession-data/csv/tunes.csv")

    id_enc, id_dec = id_mapping_from_iterable(gen.generate())
    print(id_dec)

    g = gen.generate()
    for i in range(10):
        tokens = next(g)
        print("".join(map(lambda n: str(n), tokens)))
        # print("".join([str(t) for sublist in parse_jig(tokens) for t in sublist]))

    train_gen = KerasTuneGenerator(
                    TransformGenerator(
                        gen,
                        lambda x: replace_mapping(x, id_enc)
                    ), 2, 1, len(id_dec))
    g = train_gen.generate()
    tune = []
    for i in range(150):
        n = next(g)
        if not tune:
            tune = list(n[0][0])
        else:
            tune.append(n[0][0][-1])
        if train_gen.tune_ended:
            print(n)
            print("".join(map(lambda n: str(n), replace_mapping(tune, id_dec))))

    total = 0
    for _ in gen.generate():
        total += 1
    print("Total tunes", total)


if __name__ == '__main__':
    main()
