import collections


def id_mapping_from_list(data):
    return id_mapping_from_iterable(iter(data))


def id_mapping_from_iterable(iter):
    counter = collections.Counter()

    total_tunes = 0
    for i in iter:
        total_tunes += 1
        counter.update(i)

    print("Tunes: ", total_tunes)
    count_pairs = sorted(counter.items(), key=lambda x: -x[1])

    chars, _ = list(zip(*count_pairs))
    char_ids = dict(zip(chars, range(len(chars))))
    char_ids_rev = dict(zip(char_ids.values(), char_ids.keys()))

    return char_ids, char_ids_rev


def replace_mapping(data, mapping):
    return [mapping[c] for c in data]
