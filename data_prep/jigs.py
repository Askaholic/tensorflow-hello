from generators import (
    TuneGenerator,
    TuneFilter,
    TransformGenerator
)
import os
from abc_notation import tokenize_notes, parse_jig, Note, Divider

from .mapping import id_mapping_from_iterable, replace_mapping
import itertools


def filter_jigs(tune):
    if tune[1] != "6/8":
        return False
    return True


def filter_interesting(tune):
    for c in tune[3]:
        if ord(c) > 0x7e or c == '\x14':
            return False
        if ord(c) > ord('G') and ord(c) < ord('Z'):
            return False
        if ord(c) > ord('g') and ord(c) < ord('z'):
            return False
        if c in "{}()1<>/\"*#":
            return False
    return True


def filter_dmajor(tune):
    if tune[2] == "Dmajor":
        return True
    return False


def filter_parsable(tune):
    try:
        parse_jig(tokenize_notes(tune[3]))
        return True
    except Exception:
        return False


def stringify(tune):
    return "".join([str(n) for n in tokenize_notes(tune[3]) if isinstance(n, Note) or isinstance(n, Divider)])


def is_acceptable(tune):
    if not filter_jigs(tune):
        return False
    if not filter_dmajor(tune):
        return False
    if not filter_interesting(tune):
        return False
    if not filter_parsable(tune):
        return False
    return True


def get_filtered_generator(filename):
    tune_gen = TuneGenerator(filename)
    tune_gen = TuneFilter(tune_gen, is_acceptable)
    return tune_gen


def get_tune_generator(filename):
    tune_gen = get_filtered_generator(filename)
    tune_gen = TransformGenerator(tune_gen, lambda tune: tokenize_notes(tune[3]))
    return tune_gen


def load_data(path, dataset, verbose=0):
    train_path = os.path.join(path, dataset, "train.txt")
    valid_path = os.path.join(path, dataset, "valid.txt")
    test_path = os.path.join(path, dataset, "test.txt")

    train = get_tune_generator(train_path)
    valid = get_tune_generator(valid_path)
    test = get_tune_generator(test_path)
    ids, ids_decode = id_mapping_from_iterable(
        itertools.chain(train.generate(), valid.generate(), test.generate())
    )

    train_data = TransformGenerator(train, lambda x: replace_mapping(x, ids))
    valid_data = TransformGenerator(valid, lambda x: replace_mapping(x, ids))
    test_data = TransformGenerator(test, lambda x: replace_mapping(x, ids))

    if verbose > 0:
        print("Mapping:")
        print(ids_decode)

    return train_data, valid_data, test_data, ids_decode


def main(filename, dataset_name):
    tune_gen = get_tune_generator(filename)
    dir = "data/{}".format(dataset_name)
    if not os.path.exists(dir):
        os.mkdir(dir)
    with open("data/{}/train.txt".format(dataset_name), 'w') as f:
        for tune in tune_gen.generate():
            f.write("".join(tune) + "\n")
