import tensorflow as tf
from tensorflow import keras
import numpy as np


def create_model():
    model = keras.Sequential([
        keras.layers.Flatten(input_shape=(28, 28)),
        keras.layers.Dense(128, activation=tf.nn.relu),
        keras.layers.Dropout(0.3),
        keras.layers.Dense(10, activation=tf.nn.softmax)
    ])
    model.compile(
        optimizer=tf.train.AdamOptimizer(),
        loss='sparse_categorical_crossentropy',
        metrics=['accuracy']
    )
    return model


def plot_history(history):
    plt.figure()
    plt.xlabel('Epoch')
    plt.ylabel('Accuracy')
    plt.plot(history.epoch, np.array(history.history['acc']), label="Accuracy")
    plt.plot(history.epoch, np.array(history.history['loss']), label="Loss")
    plt.legend()
    plt.ylim(0, 1)
    plt.show()


mnist = keras.datasets.fashion_mnist

class_names = ['T-shirt/top', 'Trouser', 'Pullover', 'Dress', 'Coat',
               'Sandal', 'Shirt', 'Sneaker', 'Bag', 'Ankle boot']

(train_images, train_labels), (test_images, test_labels) = mnist.load_data()
# Convert from 0-255 to 0-1
train_images = train_images / 255.0
test_images = test_images / 255.0

checkpoint_path = "training/cp.ckpt"
cp_callback = keras.callbacks.ModelCheckpoint(
    checkpoint_path,
    save_weights_only=True,
    verbose=1
)

model = create_model()
history = model.fit(train_images, train_labels, epochs=5, callbacks=[cp_callback])

test_loss, test_acc = model.evaluate(test_images, test_labels)
print("Test Accuracy: {}".format(test_acc))

model_2 = create_model()
model_2.load_weights(checkpoint_path)
test_loss, test_acc = model_2.evaluate(test_images, test_labels)
print("Loaded Model Accuracy: {}".format(test_acc))
