import tensorflow as tf
from tensorflow.examples.tutorials.mnist import input_data

mnist = input_data.read_data_sets("MNIST_data/", one_hot=True)

learning_rate = 0.5
epochs = 10
batch_size = 100


train_input = tf.placeholder(tf.float32, [None, 784])
train_output = tf.placeholder(tf.float32, [None, 10])

# Weights connecting input to hidden layer
W1 = tf.Variable(tf.random_normal([784, 300], stddev=0.03), name="W1")
b1 = tf.Variable(tf.random_normal([300]), name='b1')

# Weights connecting hidden layer to output
W2 = tf.Variable(tf.random_normal([300, 10], stddev=0.03), name="W2")
b2 = tf.Variable(tf.random_normal([10]), name='b2')

hidden_out = tf.add(tf.matmul(train_input, W1), b1)
hidden_out = tf.nn.relu(hidden_out)

y_ = tf.nn.softmax(tf.add(tf.matmul(hidden_out, W2), b2))
y_clipped = tf.clip_by_value(y_, 1e-10, 0.9999999)
cross_entropy = -tf.reduce_mean(tf.reduce_sum(train_output * tf.log(y_clipped)
                                + (1 - train_output) * tf.log(1 - y_clipped),
                                axis=1))

optimiser = tf.train.GradientDescentOptimizer(
    learning_rate=learning_rate
).minimize(cross_entropy)


init_op = tf.global_variables_initializer()

correct_preduction = tf.equal(tf.argmax(train_output, 1), tf.argmax(y_, 1))
accuracy = tf.reduce_mean(tf.cast(correct_preduction, tf.float32))

with tf.Session() as sess:
        sess.run(init_op)
        total_batch = int(len(mnist.train.labels) / batch_size)
        for epoch in range(epochs):
            avg_cost = 0
            for i in range(total_batch):
                batch_x, batch_y = mnist.train.next_batch(batch_size=batch_size)
                _, c = sess.run([optimiser, cross_entropy],
                                feed_dict={train_input: batch_x, train_output: batch_y})
                avg_cost += c / total_batch
            print("Epoch: {} cost = {:.3f}".format(epoch + 1, avg_cost))
        print(sess.run(accuracy, feed_dict={train_input: mnist.test.images, train_output: mnist.test.labels}))
